#
# Be sure to run `pod spec lint JKRedGreenXCTestObserver.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# To learn more about the attributes see http://docs.cocoapods.org/specification.html
#
Pod::Spec.new do |s|
  s.name         = "JKRedGreenXCTestObserver"
  s.version      = "1.0.0"
  s.summary      = "Display red or green indicators after running tests."
   s.description  = <<-DESC
                   JKRedGreenXCTestObsever is a simple class that can observe a XCTest test suite and display a red or green indicator on your iOS device or simulator when the tests complete.
                 
                   DESC
  s.homepage     = "https://bitbucket.org/josephkain/jkredgreenXCtestobserver/wiki/Home"

  s.license      = 'MIT'

  # Specify the authors of the library, with email addresses. You can often find
  # the email addresses of the authors by using the SCM log. E.g. $ git log
  #
  s.author       = { "Joseph Kain" => "joseph@antipodalapps.com" }

  # Specify the location from where the source should be retrieved.
  #
  s.source       = {
      :git => "git@bitbucket.org:josephkain/jkredgreenXCtestobserver.git", 
      :tag => s.version.to_s
  }
  s.source_files = '*.{h,m}'
  s.platform     = :ios
  s.framework    = 'XCTest'
  s.xcconfig     = { 'FRAMEWORK_SEARCH_PATHS' => '"$(SDKROOT)/Developer/Library/Frameworks" "$(DEVELOPER_LIBRARY_DIR)/Frameworks"' }
end

# Description

JKRedGreenXCTestObsever is a simple class that can observe a XCTest test suite and display a red or green indicator on your iOS device or simulator when the tests complete.
